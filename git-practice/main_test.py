import pytest
from main import hello


def test_hello():
    assert hello() == 'Hello World!'


def test_hello_2():
    assert '!' in hello()


def test_hello_3():
    assert 'Hello' in hello()